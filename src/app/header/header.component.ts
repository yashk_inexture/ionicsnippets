import { Component, OnInit } from '@angular/core';
import { CanActivate, Router,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit 
{
	isLogin = false;
	constructor(private router:Router) 
	{ 
		if(localStorage.getItem("userLogin")!=null)
		{
			this.isLogin = true;
		}
	}

	ngOnInit() 
	{
	}
	logout()
	{
		localStorage.removeItem("userLogin");
		this.router.navigate(['/login']);
	}

}
