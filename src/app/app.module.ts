import { BrowserModule } from '@angular/platform-browser';
import { NgModule,ApplicationRef } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ReactiveFormsModule } from '@angular/forms';
import { RecaptchaModule } from 'ng-recaptcha';

import { CommonService } from './services/common.service';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './pages/login/login.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { HttpModule } from '@angular/http';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { CustomFormsModule } from 'ng2-validation';
import { ToastyModule} from 'ng2-toasty';
import { UserComponent } from './pages/user/user.component';
//import { SnippetDetailComponent } from './pages/snippet-detail/snippet-detail.component';
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    UserComponent,
    //SnippetDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    CustomFormsModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaModule.forRoot(),
    ToastyModule.forRoot(),
  ],
  exports : [ToastyModule],
  providers: [CommonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
