import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { UserComponent } from './pages/user/user.component';
//import { SnippetDetailComponent } from './pages/snippet-detail/snippet-detail.component';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: DashboardComponent },
    { path: 'login', component: LoginComponent },
    { path: 'user', component: UserComponent },
    { path: 'sign-up', component: SignUpComponent },
    //{ path: 'snippet-detail/:snippetId', component: SnippetDetailComponent }
    // { path: 'change-password', component: ChangePasswordComponent },
    // { path: 'reset-password', component: ResetPasswordComponent},
    // { path: 'profile', component: ProfileComponent},
    // { path: 'edit-profile', component: EditProfileComponent},
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
