import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { Http, Headers, RequestOptions } from '@angular/http';
import { Http, Response,Headers, RequestOptions} from '@angular/http';
@Injectable()
export class CommonService 
{
	// url = "http://yash.ubiquitousdevelopers.com/api_lumen/api/";
	url = "http://localhost/ionicsnippets/admin/public/api/";
	constructor(public http: Http) 
	{
		console.log('Hello CommonProvider Provider');
	}
	requestPost(data)
	{
		// console.log(data);
		// this.loadingShow();
		return new Promise(resolve => 
		{
			let headers = new Headers({
				'Content-Type': 'application/x-www-form-urlencoded'
			});

			let options = new RequestOptions({
				headers: headers
			});

			let body = "";

			for (var key in data)
			{
				body += key+"="+data[key]+"&";
			}

			console.log(data.methodName);
			this.http.post(this.url+data.methodName, body ,options)
			.subscribe(data => 
			{ 
				// this.loadingHide();
				resolve(JSON.parse(data['_body']));

			},
			error => 
			{
				// this.loadingHide();
				resolve({"error":"404"});
			});
		});
	}
	requestGet(url) 
	{
		// this.loadingShow();
		return new Promise(resolve => 
		{
			let headers = new Headers({
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control' : 'no-cache'
			});

			let options = new RequestOptions({
				headers: headers
			});

			this.http.get(url)
			// .map(res => res.json())
			.subscribe(data => 
			{
				resolve(data);
				// this.loadingHide();
			});
		});
	}

}
