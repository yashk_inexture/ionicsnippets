import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit 
{
	data = {"methodName":""};
	detail = [];
	constructor(public CommonCtrl:CommonService) 
	{ 
		this.data.methodName = "snippets";
		this.CommonCtrl.requestPost(this.data).then(res=>{
			this.detail = res['extras'];
			console.log(this.detail);
		})
	}

	ngOnInit() {
	}

	viewDetail(id)
	{
		console.log(id);
	}

}
