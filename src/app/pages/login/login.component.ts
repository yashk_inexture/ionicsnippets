import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CustomValidators } from 'ng2-validation';
import { CommonService }  from '../../services/common.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';
import { CanActivate, Router,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit 
{
	public userform: FormGroup;
	data = {};
	rdata :any;
	is_error = false;
	is_success = false;
	error_message =  '';
	constructor(private fb: FormBuilder, private apiService: CommonService, private toastyService:ToastyService, private toastyConfig: ToastyConfig , private router:Router) 
	{ }

	ngOnInit() 
	{
		this.userform = this.fb.group({
		   email : ['new@gmail.com',Validators.compose([Validators.required,CustomValidators.email])],
		   password : ['123123',Validators.compose([Validators.required,Validators.minLength(6)])]
		});
	}

	usersigninSubmit(form : NgForm)
    {
    	console.log(form.value);
    	form.value.methodName = "loginapi";
    	this.apiService.requestPost(form.value).then(res =>
        {
            this.rdata = res;
            console.log(this.rdata);
            if(this.rdata['status'] == 1)
            {
            	localStorage.setItem("userLogin",JSON.stringify(this.rdata['extras']));
            	var toastOptions:ToastOptions = {
                    title: this.rdata['message'],
                    showClose: true,
                    timeout: 5000,
                    theme: 'default'
                };
                this.toastyService.success(toastOptions);
                this.router.navigate(['/user']);
            }
            else
            {
            	var toastOptions:ToastOptions = {
                    title: this.rdata['message'],
                    showClose: true,
                    timeout: 5000,
                    theme: 'default'
                };
                this.toastyService.error(toastOptions);
            }
        },
        error => {
            this.rdata = JSON.parse(error._body);
            this.is_error = true;
            this.is_success = false;
            this.error_message = this.rdata['message'];
        }
        );
    }

}
