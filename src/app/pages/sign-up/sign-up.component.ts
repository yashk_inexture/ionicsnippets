import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CustomValidators } from 'ng2-validation';
import {RecaptchaModule, RECAPTCHA_SETTINGS} from 'ng-recaptcha';
import {RecaptchaFormsModule} from 'ng-recaptcha/forms';
import { CommonService }  from '../../services/common.service';
import { CanActivate, Router,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

const n_password = new FormControl('', [Validators.required,Validators.minLength(6)]);
const cn_password = new FormControl('', [CustomValidators.equalTo(n_password),Validators.minLength(6)]);
//const recaptchaReactive = new FormControl(null, Validators.required);
export declare var grecaptcha: any;
@Component({
	selector: 'app-sign-up',
	templateUrl: './sign-up.component.html',
	styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit 
{
	public userSignupform: FormGroup;
	data = {};
	rdata :any;
    is_error = false;
    error_message =  '';
    success_message = '';
    is_success = false;
	constructor(private toastyService:ToastyService, private toastyConfig: ToastyConfig,private fb:FormBuilder, private apiService: CommonService, private router:Router) 
	{

	}

	ngOnInit() 
	{
		this.userSignupform = this.fb.group({
		   name : ['',Validators.compose([Validators.required])],
		   email : ['',Validators.compose([Validators.required,CustomValidators.email])],
		   password : n_password,
		   cpassword : cn_password,
		   gender : ['',Validators.compose([Validators.required])],
		   phone : ['',Validators.compose([Validators.required,CustomValidators.digits,Validators.minLength(5), Validators.maxLength(10)])],
		   address : ['',Validators.compose([Validators.required])]
		  // recaptcha : ['',Validators.compose([Validators.required])]
		});
	}

	resolved(captchaResponse: string) {
        console.log(`Resolved captcha with response ${captchaResponse}:`);
    }

    usersignupSubmit(form : NgForm)
    {
    	console.log(form.value);
    	form.value.methodName = "registerapi";
		this.apiService.requestPost(form.value).then(res =>
        {
            this.rdata = res;
            console.log(this.rdata);
            if(this.rdata['status'] == 1)
            {
            	var toastOptions:ToastOptions = {
                    title: this.rdata['message'],
                    showClose: true,
                    timeout: 5000,
                    theme: 'default'
                };
                this.toastyService.success(toastOptions);
                this.router.navigate(['/login']);
            }
            else
            {
            	var toastOptions:ToastOptions = {
                    title: this.rdata['message'],
                    showClose: true,
                    timeout: 5000,
                    theme: 'default'
                };
                this.toastyService.error(toastOptions);
            }
        },
        error => {
            this.rdata = JSON.parse(error._body);
            this.is_error = true;
            this.is_success = false;
            this.error_message = this.rdata['message'];
        }
        );
    }
}
