<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSnippetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('snippets'))
        {  
            Schema::create('snippets', function (Blueprint $table)
            {
                $table->increments('id');
                $table->string('title',255);
                $table->string('photo', 255)->nullable();
                $table->longText('description', 255);
                $table->longText('html', 255);
                $table->longText('css', 255);
                $table->timestamps();
                $table->string('created_by', 36)->nullable();
                $table->string('updated_by', 36)->nullable();
                $table->tinyInteger('status')->default(1)->nullable()->comment('1 - Active , 2 - Inactive, 3 - Deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
