@extends('Admin.master')

@section('content')
<section class="content-header">
    <h1>
        {{trans('labels.snippets')}}
        <div class="pull-right">
            <a href="{{ url('admin/addsnippets') }}" class="btn btn-block btn-primary add-btn-primary pull-right">{{trans('labels.add')}}</a>
        </div>
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{trans('labels.snippet_list')}}</h3>
                </div>
                <div class="box-body">
                    <table id="listContest" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{trans('labels.title')}}</th>
                                <th>{{trans('labels.description')}}</th>
                              <!--   <th>{{trans('labels.html')}}</th>
                                <th>{{trans('labels.css')}}</th> -->
                                <th>{{trans('labels.photo')}}</th>
                                <th>{{trans('labels.snippet_list_action_label')}}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')

<script>
    var getContestList = function(ajaxParams) {
        $("#listContest").DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "ajax":{
                "url": "{{ url('admin/list-snippet-ajax') }}",
                "dataType": "json",
                "type": "POST",
                headers: { 
                    'X-CSRF-TOKEN': "{{ csrf_token() }} "
                },
                "data" : function(data) {
                    if (ajaxParams) {
                        $.each(ajaxParams, function(key, value) {
                            data[key] = value;
                        });
                        ajaxParams = {};
                    }
                }
            },
            "columns": [
                { "data": "title" },
                { "data": "description" },
               /* { "data": "html" },
                { "data": "css" }, */
                { "data": "photo" },
                { "data": "action", "orderable": false }
            ], 
            "initComplete": function(settings, json) {
                if(typeof(json.customMessage) != "undefined" && json.customMessage !== '') {
                    $('.customMessage').removeClass('hidden');
                    $('#customMessage').html(json.customMessage);
                }
            }
        });
    };
    $(document).ready(function () {
        var ajaxParams = {};
        getContestList(ajaxParams);
        // Remove user
        $(document).on('click', '.btn-cancel-contest', function(e){
            e.preventDefault();
            var contestId = $(this).attr('data-id');
            var cmessage = 'Are you sure you want to Cancel this Contest ?';
            var ctitle = 'Cancel Contest';

            ajaxParams.customActionType = 'groupAction';
            ajaxParams.customActionName = 'cancel';
            ajaxParams.id = [contestId];

            bootbox.dialog({
                onEscape: function () {},
                message: cmessage,
                title: ctitle,
                buttons: {
                    Yes: {
                        label: 'Yes',
                        className: 'btn green',
                        callback: function () {
                            getContestList(ajaxParams);
                        }
                    },
                    No: {
                        label: 'No',
                        className: 'btn btn-default'
                    }
                }
            });
        });
    });
</script>
@endsection
