@extends('Admin.master')

@section('content')
<div class="login-box">
  <div class="login-box-body">
    @if(isset($forgotToken) && $forgotToken != '')
      <p class="login-box-msg">Set New Password</p>
      <form action="{{ url('/admin/resetPassword') }}" method="post" enctype="form-multipart/form-data" id="reset_password_form">
        {{csrf_field()}}
        <div class="form-group has-feedback">
          <input type="hidden" name="forgot_token" value="{{$forgotToken}}" />
          <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
              <input type="password" class="form-control" id="password" name="password" placeholder="New Password" value="{{ old('password') }}">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
          </div>
        </div>
        <div class="row">
          <div class="col-xs-7">

          </div>
          <div class="col-xs-5">
            <button type="submit" class="btn btn-primary btn-block btn-flat" id="reset_password_submit">Set Password</button>
          </div>
        </div>
      </form>
    @else
      <div class="panel-heading"><center>Reset Password request not found</center></div><br/>
    @endif
    <a href="{{url('admin')}}" class="text-center">Login</a>
  </div>
</div>
@endsection

@section('script')
  <script src="{{ asset('js/front/jquery.validate.min.js') }}"></script>
  <script type="text/javascript">
    jQuery(document).ready(function() {
      var passwordRules = {
          password : {
            required : true,
            minlength: 6,
            maxlength : 20,
          }
      };
      $("#reset_password_form").validate({
          rules: passwordRules,
          messages: {
              password: {
                  required: "Password is required",
                  maxlength: 'Password min length is 6',
                  minlength: 'Password min length is 20'
              },
          }
      });
      $("#reset_password_submit").click(function(){
          var form = $("#reset_password_form");
          form.validate();
          if(form.valid())
          {
              form.submit();
              $("#reset_password_submit").attr("disabled", 'disabled');
          }
          else
          {
              $("#reset_password_submit").removeAttr("disabled", 'disabled');
          }
      });

    });
  </script>
@endsection