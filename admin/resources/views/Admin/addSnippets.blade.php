@extends('Admin.master')

@section('content')
<section class="content-header">
    <h1>
        {{trans('labels.add_snippets')}}
        <div class="pull-right">
            <!-- <a href="{{ url('admin/addsnippets') }}" class="btn btn-block btn-primary add-btn-primary pull-right">{{trans('labels.add')}}</a> -->
        </div>
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{trans('labels.add_snippets')}}</h3>
                </div>
                <form class="form-horizontal" id="addsnippets" enctype="multipart/form-data" method="POST" action="{{ url('admin/save-snippets') }}">
                    {{ csrf_field() }} 
                    <div class="box-body">
                        <input type="hidden" name="id" value="">
                         <input type="hidden" name="hidden_image" value="<?php echo (isset($editSnippets) && !empty($editSnippets)) ? $editSnippets->photo : ''; ?>">
                        <div class="form-group">
                            <label for="name" class="col-md-2 control-label">{{ trans('labels.title') }}</label>
                            <div class="col-md-6 ">
                                <input type="text" class="form-control" name="title" id="title" placeholder="{{ trans('labels.title') }}" value=""/>                        
                            </div>
                        </div>                      
                        <div class="form-group">
                            <label class="col-md-2 control-label">{{ trans('labels.description') }}</label>
                            <div class="col-md-6">
                                <textarea class="form-control"  name="description" id="no_secs_display" placeholder="{{ trans('labels.description') }}" rows="4"></textarea>
                            </div>
                            
                        </div>
                        <div class="form-group">
                                <label for="banner" class="col-md-2 control-label">{{ trans('labels.photo')}}</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control" id="file" name="file">
                                    <?php
                                    if (isset($editSnippets->id) && $editSnippets->id != '0') {
                                        if (File::exists(public_path($snippetsUploadImage . $editSnippets->file)) && $editSnippets->file != '') { ?>
                                            <img src="{{ url($snippetsUploadImage.$editSnippets->file) }}" alt="{{$editSnippets->file}}"  height="70" width="70">
                                        <?php } else { ?>
                                            <img src="{{ asset('/uploads/default.png')}}" class="user-image" alt="Default Image" height="70" width="70">
                                            <?php }
                                    }
                                    ?>
                                    
                                </div>
                            </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">{{ trans('labels.html') }}</label>
                            <div class="col-md-8">
                                <textarea class="form-control ckeditor" name="html" id="html" placeholder="{{ trans('labels.html') }}" rows="4"></textarea>
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">{{ trans('labels.css') }}</label>
                            <div class="col-md-8">
                                <textarea class="form-control ckeditor"  name="css" id="css" placeholder="{{ trans('labels.css') }}" rows="4"></textarea>
                            </div>
                            
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="form-group">
                            <div class="col-md-1 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">{{ trans('labels.submit') }}</button>
                            </div>
                            <div class="col-md-2">
                                <a href="{{url('admin/snippets')}}" class="btn btn-primary">{{ trans('labels.cancel') }}</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
<script src="{{url('ckeditor/ckeditor.js')}}"></script>

<script type="text/javascript">

    CKEDITOR.replace('html');
    CKEDITOR.replace('css');

    $(document).ready(function () {
        $.validator.addMethod("emptyhtml", function(value, element) {
            var cms_body_data = CKEDITOR.instances['html'].getData();

          return cms_body_data != '';
        }, "HTML is required.");

        $.validator.addMethod("emptycss", function(value, element) {
            var cms_body_data = CKEDITOR.instances['css'].getData();

          return cms_body_data != '';
        }, "CSS is required.");

        var addSnippetRules = {
            title: {
                required: true
            },
            description: {
                required: true

            },
            html: {
                emptyhtml: true
            },
            css:  {
                emptycss: true
            },
            file: {
                // required: true,
                extension: "webm|mkv|flv|vob|ogv|ogg|wmv|asf|amv|mp4|m4p|m4v|m4v|3gp|3g2|f4v|f4p|f4a|f4b|mpeg|mpg|m2v|mpv|mpe|png|jpeg|jpg|bmp",
                // filesize: 1024,
            }
        };
        $("#addsnippets").validate({
            ignore: "",
            rules: addSnippetRules,
        });
    });
</script>
@endsection
