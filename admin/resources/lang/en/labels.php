<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Web Page Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during rendering application for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'appname' => 'Ionic Snippets',
    'appshortname' => 'Ionic Snippets',
    'admin' => 'Ionic Snippets',
    'online' => 'Online',
    'startsession' => 'Sign in to start your session',
    'emaillbl' => 'Email',
    'passwordlbl' => 'Password',
    'dashboard' => 'Dashboard',
    
    'login' => 'Login',
    'logout' => 'Logout',  
    //button
    'add' => 'Add',
    'submit' => 'Submit',
    'save' => 'Save',
    'cancel' => 'Cancel',

    //snippets
    'snippets' => 'Snippets',
    'add_snippets' => 'Add Snippets',
    'title' => 'Snippet Title',
    'description' => 'Snippet Description',
    'photo' => 'Snippet Image',
    'html' => 'Snippet Html',
    'css' => 'Snippet CSS',

    'snippet_list' => 'Snippet List',
    'snippet_list_action_label' => 'Action',


    'copyrightstr' => 'Copy Rights',
    'version' => 'Version 1.0'

];