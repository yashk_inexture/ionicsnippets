<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Admin labels line
      |--------------------------------------------------------------------------
     */

    'common_error_msg'          => 'Something went wrong. Please try again.',
    /*
     * User Module messages
     */
    'delete_user'               => 'User deleted Successfully.',
    'user_not_exist'            => 'User does not exist.',
    'not_authorized'            => 'You are not authorized to do this action.',
    'user_updated_success'      => 'User has been updated successfully.',
    'snippets_created_success'      => 'Snippets has been created successfully.',
    /*
     * Game Module messages
     */
    'delete_snippets'               => 'Snippets deleted Successfully.',
    'snippets_not_exist'            => 'Snippets does not exist.',
    'not_authorized'            => 'You are not authorized to do this action.',
    'snippets_updated_success'      => 'Snippets has been updated successfully.',
    'snippets_created_success'      => 'Snippets has been created successfully.',

    /**
     * Item Module Messages
     */
    'delete_item'               => 'Item Deleted Successfully.',
    'item_updated_success'      => 'Item has been updated successfully.',
    'item_created_success'      => 'Item has been created successfully.',

    /*
     * Gamecase Module Messages
     */
    'delete_gamecase' => 'Boost Pack Item Deleted Successfully.',
    'gamecase_updated_success' => 'Boost Pack Item has been updated successfully.',
    'gamecase_created_success' => 'Boost Pack Item has been created successfully.',
    'not_authorized'            => 'You are not authorized to do this action.',

    /*
     * Game Case Module Messages
     */
    'delete_gamecase_bundle' => 'Boost Pack Bundle Deleted Successfully.',
    'gamecase_bundle_updated_uccess' => 'Boost Pack Bundle has been updated successfully.',
    'gamecase_bundle_created_success' => 'Boost Pack Bundle has been created successfully.',
    'gamecase_not_exist' => 'Boost Pack Bundle does not exist.',
    'gamecase_upload_image' => 'Please Upload Only Image',
    /*
     * Contest Module messages
     */
    'contest_not_exist'         => 'Contest does not exist.',
    'contest_data_not_exist'    => 'Contest data does not exist.',
    'contest_updated_success'   => 'Contest has been updated successfully',
    'contest_score_updated_success'   => 'Contest score has been updated successfully',
    'contest_created_success'   => 'Contest has been created successfully',
    'cancel_contest'            => 'Contest has been cancelled successfully',
    'image_mail'                => 'Mail sent successfully',
    'complete_contest'            => 'Contest has been completed successfully',
    'image_rejected'            => 'Image Rejected successfully',
    'reupload_request'            => 'Re-upload request sent Successfully',
    /*
     * Ads Module Messages
     */
    'ads_not_exist'             => 'Ads does not exist.',
    'ads_updated_success'       => 'Ads has been updated successfully',
    'ads_created_success'       => 'Ads has been created successfully',
    'delete_ads'                => 'Ads deleted Successfully.',
    'default_ads'               => 'Deafult ad Set Successfully',
    /*
     * Roster Module messages
     */
    'delete_roster'             => 'Roster deleted Successfully.',
    'roster_not_exist'          => 'Roster does not exist.',
    'roster_updated_success'    => 'Roster has been updated successfully',
    'roster_created_success'    => 'Roster has been created successfully',
    
    /*
     *  Players Module Messages
     */
    'delete_players'             => 'Players deleted Successfully.',
    'players_not_exist'          => 'Players does not exist.',
    'players_updated_success'    => 'Players has been updated successfully',
    'players_created_success'    => 'Players has been created successfully',
    'player_game_created_success'=> 'Player\'s game has been created successfully',
    'player_game_updated_success'=> 'Player\'s game has been updated successfully',
    'delete_players_game'        => 'Player\'s game deleted Successfully.',
    'player_game_exist'          => 'Player already participated in this game',

    /*
     *  Team Module Messages
     */
    'delete_team'             => 'Team deleted Successfully.',
    'team_not_exist'          => 'Team does not exist.',
    'team_updated_success'    => 'Team has been updated successfully',
    'team_created_success'    => 'Team has been created successfully',
    'team_game_created_success'=> 'Team\'s game has been created successfully',
    'team_game_updated_success'=> 'Team\'s game has been updated successfully',
    'delete_team'              => 'Team\'s deleted Successfully.',

    /*
     *  Players Module Messages
     */
    'roster_player_added_success'       => 'Player has been added successfully for this Roster',
    'delete_roster_player'              => 'Player has been deleted from roster',
    
    
    /*
     * Prize Distribution Ratio and Plan Module Messages
     */
    'prize_not_exist'             => 'Prize Distribution Plan does not exist.',
    'prize_updated_success'       => 'Prize has been updated successfully',
    'prize_created_success'       => 'Prize has been created successfully',
    'delete_prize'                => 'Prize deleted Successfully.', 
    
    /*
     * Contest Type Module Messages
     */
    'contest_type_not_exist'             => 'Contest Type does not exist.',
    'contest_type_updated_success'       => 'Contest Type has been updated successfully',
    'contest_type_created_success'       => 'Contest Type has been created successfully',    
];
