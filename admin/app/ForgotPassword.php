<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ForgotPassword extends Authenticatable
{

    protected $table = 'forgot_password';
    
    protected $guarded = [];

    public function savePasswordRequestDetail($data)
    {
      $checkRequestExistOrNot = $this->where( ['users_id' => $data['users_id'] , 'users_type' => $data['users_type'] ])->first();

      if($checkRequestExistOrNot)
      {
          $savePasswordDetail = $this->where('id', $checkRequestExistOrNot->id)->update($data);
      }
      else
      {
          $savePasswordDetail = $this->insert($data);
      }
      return $savePasswordDetail;
    }

    public function getExistTokenDetail($forgotToken, $userType)
    {
        if($forgotToken != "" && in_array($userType, ['1', '2']))
        {
          $tokenDetail = $this->where(['forgot_token' => $forgotToken, 'is_active' => 1, 'users_type' => $userType])->first();
        }
        else
        {
          $tokenDetail = [];
        }

        return $tokenDetail;
    }
}

