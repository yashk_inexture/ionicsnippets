<?php
namespace App\Http\Controllers\Api;
use App\User;
use App\Snippets;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Auth;
use File;
use Mail;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;


class ExampleController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
    //
    }
    public function index()
    {

    }
    //register
    public function register()
    {
        $data = Input::All();
        //print_r($data);die;
        /*$fileName="";
        if (Input::file())
        {
            $file = Input::file('propic');
            if (!empty($file)) 
            {
                $fileName = 'user' . time() . '.' . $file->getClientOriginalExtension();

                $path = 'E:/angular/api_lumen/public/upload/user/';
                Image::make($file->getRealPath())->save($path.$fileName);
            }
        }

        $data['propic']=$fileName;*/

        if( $data['gender'] == "male" ) {
            $data['gender'] = 1;
        }
        else {
            $data['gender'] = 2;
        }

        $modelobj = new User();
        $userData = $modelobj->register($data);
        if($userData)
        {
            return response()->json(['status' => 1 ,'extras' => $userData,'message' => 'Register successfully.']);
        }
        else
        {
            return response()->json(['status' => 0 ,'message' => 'Email already exists.']);
        }

    }
    public function login()
    {
        $data = Input::All();
        
        $modelobj = new User();
        $userData = $modelobj->login($data);
        
        //$user = User::where('email', $data['email'])->first();
        if($userData)
        {
            return response()->json(['status' => 1,'extras' => $userData,'message' => 'Login successfully.']);
        }
        else
        {
            return response()->json(['status' => 0 ,'message' => 'Wrong Username Or password.']);
        }
    }
    public function forgotPassword()
    {
        $email = Input::All();
        // print_r($email);die;

        $user = User::where('email',$email['email'])->first();
        // print_r($user);die;
        if(isset($user) && !empty($user))
        {
            // --------------------start sending mail -----------------------------//
            $replaceArray = array();
            $replaceArray['USER_NAME'] = $user['name'];
            $id = $user['id'];
            // $replaceArray['USER_UNIQUEID'] = Helpers::getUserUniqueId();
            // $replaceArray['USER_URL'] = url("resetUserPassword" . "/" . $replaceArray['USER_UNIQUEID']);

            // $emailTemplateContent = $this->TemplateRepository->getEmailTemplateDataByName(Config::get('constant.USER_RESET_EMAIL_TEMPLATE_NAME'));
            // $content = $this->TemplateRepository->getEmailContent($emailTemplateContent->et_body, $replaceArray);
            $data = array();
            $data['subject'] = "subject";
            $data['toEmail'] = $user['email'];
            $data['toName'] = $user['name'];
            $data['content'] = "content";
            // $data['USER_UNIQUEID'] = $replaceArray['USER_UNIQUEID'];
            // $data['user_url'] = $replaceArray['USER_URL'];
            $data['user_id'] = $id;
            // $data['is_agent'] = $isAgent;
            Mail::send(['html' => '<html><body><h6>hello</h6></body></html>'], $data, function($message) use ($data) {
                $message->subject($data['subject']);
                $message->to($data['toEmail'], $data['toName']);
                // $useruniqueid = [];
                // $useruniqueid['urp_uniquecode'] = $data['USER_UNIQUEID'];
                // $useruniqueid['urp_user_id'] = $data['user_id'];
                // $useruniqueid['urp_is_agent'] = $data['is_agent'];
                // $this->UserRepository->addUserResetPassword($useruniqueid);
            });
        }           // ------------------------end sending mai

        die;

        $modelobj = new User();
        $userData = $modelobj->forgotPassword($email);
        // $user = User::where('email', $email)->first();
        if($user)
        {
            return response()->json(['status' => 1,'extras' => $user]);
        }
        else
        {
            return response()->json(['status' => 0],401);
        }
    }
    public function profile(Request $request)
    {
        $id = $request->input('id');
        $modelobj = new User();
        $userData = $modelobj->getUserData($id);
        if($userData)
        {
            return response()->json(['status' => 1,'extras' => $userData]);
        }
        else
        {
            return response()->json(['status' => 0],401);
        }
    }
    public function editprofile()
    {
        $value = Input::All();

        $modelobj = new User();
        $editUserData = $modelobj->editUserData($value);

        if($editUserData == true)
        {
            return response()->json(['status'=>1]);    
        }
        else
        {
            return response()->json(['status'=>0],401);
        }
    }
    public function changePassword()
    {
        $pass = Input::All();
        //print_r($pass);die;
        $user = User::where('id', $pass['id'])->first();
        $cpass = Hash::make($pass['npassword']);
        //print_r($user);die;
        if(Hash::check($pass['cpassword'], $user->password))
        {
            User::where('id', $pass['id'])->update(['password' => $cpass]);
            return response()->json(['status' => 1]);
        }
        else
        {
            return response()->json(['status' => 0],401);
        }
    }

    public function getTask()
    {
        $modelobj = new User();
        $taskData = $modelobj->getTaskDetails();  
        if($taskData)
        {
            return response()->json(['status' => 1,'extras' => $taskData]);
        }
        else
        {
            return response()->json(['status' => 0],401);
        } 
    }
    public function addTask()
    {
        $task = Input::All();
        $modelobj = new User();
        $taskData = $modelobj->addTaskData($task);

        if($taskData==true)
        {
            return response()->json(['status'=>1]);    
        }
        else
        {
            return response()->json(['status'=>0],401);
        }
    }
    public function getTaskDetails(Request $request)
    {
        // print_r($request->input('id'));
        $id = $request->input('id');
        $modelobj = new User();
        $userData = $modelobj->getTaskData($id);
        //print_r($userData);die;
        if($userData)
        {
            return response()->json(['status' => 1,'extras' => $userData]);
        }
        else
        {
            return response()->json(['status' => 0],401);
        }
    }
    public function edittask()
    {
        $value = Input::All();
        $modelobj = new User();
        $editTaskData = $modelobj->editTaskData($value);

        if($editTaskData == true)
        {
            return response()->json(['status'=>1]);    
        }
        else
        {
            return response()->json(['status'=>0],401);
        }
    }
    public function deletetask()
    {
        $id = Input::All();
        $modelobj = new User();
        $deleteData = $modelobj->deleteTaskData($id);

        if($deleteData == true)
        {
            return response()->json(['status'=>1]);    
        }
        else
        {
            return response()->json(['status'=>0],401);
        }
    }

}