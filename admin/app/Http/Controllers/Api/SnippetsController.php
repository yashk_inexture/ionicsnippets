<?php
namespace App\Http\Controllers\Api;
use App\User;
use App\Snippets;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Auth;
use File;
use Mail;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Config;


class SnippetsController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{
		//$this->middleware('auth');
		$this->snippetsOriginalImageUploadPath = Config::get('constant.SNIPPETS_ORIGINAL_IMAGE_UPLOAD_PATH');
	}
	public function snippets()
	{
		$modelobj = new Snippets();
		$snippetsData = $modelobj->getSnippetsData();

		if(!is_null($snippetsData)) {
			foreach ($snippetsData as $snippets) {
				$snippets->photo = ($snippets->photo != NULL && $snippets->photo != '') ? url($this->snippetsOriginalImageUploadPath.$snippets->photo) : '';
			}
		}
		//print_r($taskData);die;  
		if($snippetsData)
		{
			return response()->json(['status' => 1,'extras' => $snippetsData]);
		}
		else
		{
			return response()->json(['status' => 0],401);
		} 
	}

}