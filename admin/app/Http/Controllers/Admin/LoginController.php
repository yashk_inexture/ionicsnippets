<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\AdminUser;
use App\ForgotPassword;
use Input;
use Mail;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->objAdminUser = new AdminUser();
    }

    public function showLoginForm()
    {  
        if(Auth::guard('admin')->check())
        {
            return Redirect::to('admin/home');
        }
        return view('Admin.login');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        return redirect('/admin');
    }

    public function forgotPassword()
    {
        if(Auth::guard('admin')->check())
        {
            return Redirect::to('admin/home');
        }
        return view('Admin.forgotPassword');
    }

    public function sendPassword()
    {
        if(Auth::guard('admin')->check())
        {
            return Redirect::to('/admin/home');
        }

        $email = trim(e(Input::get('email')));
        $objAdminUser = new AdminUser();

        $getUserDetail = $objAdminUser->getActiveUserDetailByEmail($email);
        $objForgotPassword = new ForgotPassword();
        if($getUserDetail)
        {
            $randomStr = str_random(25);
            $data = array();
            $data['id'] = 0;
            $data['users_id'] = $getUserDetail->id;
            $data['forgot_token'] = $randomStr;
            $data['users_type'] = 1;
            $data['is_active'] = 1;

            $savePasswordRequestDetail = $objForgotPassword->savePasswordRequestDetail($data);

            $data['first_name'] = $getUserDetail->name;
            $data['email'] = $getUserDetail->email;

            $content = 'Hi <strong>'.$data['first_name'].'</strong>, <br/><br/> Please click on the link below to Reset your Password. <strong></strong> <br/><br/><a href=' . url("admin/resetPassword/" . $data['forgot_token']) . '>'. url("admin/resetPassword/" . $data['forgot_token']) .'</a>';

            $data['content'] = $content;

            Mail::send(['html' => 'emails.PasswordResetLink'], $data, function($message) use ($data) {
                    $message->subject('Password Reset Request');
                    $message->to($data['email'], $data['first_name']);
                });
            return Redirect::to('/admin/forgotPassword')->with('success',"Send forgot password link to your register email id");
        }
        else
        {
            return Redirect::to('/admin/forgotPassword')->with('error',"Email Id does not exist")->withInput();
        }
    }

    public function resetPassword($forgotToken)
    {
        $userType = 1; //Admin
        $objForgotPassword = new ForgotPassword();
        $getExistTokenDetail = $objForgotPassword->getExistTokenDetail($forgotToken, $userType);

        if($getExistTokenDetail && $forgotToken != '')
        {
            return view('Admin.resetPassword', compact('forgotToken'));
        }
        else
        {
            return view('Admin.resetPassword', compact('forgotToken'));
        }
    }

    public function setNewPassword()
    {
        $forgot_token_input = Input::get('forgot_token');
        $objForgotPassword = new ForgotPassword();
        $objAdminUser = new AdminUser();
        $userType =  1; //Admin
        $getExistTokenDetail = $objForgotPassword->getExistTokenDetail($forgot_token_input, $userType);
        if($getExistTokenDetail)
        {
            $newPassword = bcrypt(Input::get('password'));
            $userDetail = $objAdminUser->getActiveUserDetailById($getExistTokenDetail->users_id);

            if($userDetail)
            {
                $userDetailUpdate = AdminUser::where(['id'=>$getExistTokenDetail->users_id])->update(['password'=>$newPassword]);

                if(Auth::guard('admin')->attempt(['email' => $userDetail->email, 'password' => Input::get('password'), 'deleted'=>1]))
                {
                    $setNewPassword = ForgotPassword::where(['forgot_token'=>$forgot_token_session])->update(['forgot_token'=>"", 'is_active' => 2]);
                    return Redirect::to('/admin/home')->with('success',trans('passwords.reset'));
                }
                else
                {
                    return Redirect::to('/admin')->with('error',trans('passwords.something_wrong'));
                }
            }
            else
            {
                return Redirect::to('/admin')->with('error',trans('passwords.not_user'));
            }
        }
        else
        {
            return Redirect::to('/admin')->with('error',trans('passwords.no_request'));
        }
    }
}
