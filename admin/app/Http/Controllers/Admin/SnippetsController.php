<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Snippets;
use Auth;
use Config;
use Input;
use Response;
use Image;
use File;
use Redirect;
class SnippetsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
        $this->objSnippets = new Snippets();
        $this->snippetOriginalImageUploadPath = Config::get('constant.SNIPPETS_ORIGINAL_IMAGE_UPLOAD_PATH');
        $this->snippetThumbImageUploadPath = Config::get('constant.SNIPPETS_THUMB_IMAGE_UPLOAD_PATH');
        $this->snippetsThumbImageHeight = Config::get('constant.SNIPPETS_THUMB_IMAGE_HEIGHT');
        $this->snippetsThumbImageWidth = Config::get('constant.SNIPPETS_THUMB_IMAGE_WIDTH');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.snippets');
    }
    public function addSnippets()
    {
        return view('Admin.addSnippets');
    }

    public function listSnippetAjax()
    {
        $records = array();
        //processing custom actions
        if (Input::get('customActionType') == 'groupAction') {

            $action = Input::get('customActionName');
            $idArray = Input::get('id');

            switch ($action) {
                case "delete":
                foreach ($idArray as $_idArray) {
                    $snippets = Snippets::find($_idArray);
                    if($snippets) {
                        $imageOriginal = ($snippets->photo != '' && $snippets->photo != null) ? public_path($this->snippetOriginalImageUploadPath . $snippets->photo) : '';
                        $imageThumb = ($snippets->photo != '' && $snippets->photo != null) ? public_path($this->snippetThumbImageUploadPath . $snippets->photo) : '';
                        
                        
                        // Unlink original image if exists
                        if (!empty($imageOriginal) && file_exists($imageOriginal)) {
                            File::delete($imageOriginal);
                        }
                        
                        // Unlink thumb image if exists
                        if (!empty($imageThumb) && file_exists($imageThumb)) {
                            File::delete($imageThumb);
                        }
                        
                        $snippets->delete();
                    }
                    $records["customMessage"] = trans('adminmsg.delete_ads');
                }
            }
        }

        $columns = array(
            0 => 'title',
            1 => 'description',
          /*  2 => 'html',
            3 => 'css',*/
            2 => 'photo'
        );

        $order = Input::get('order');
        $search = Input::get('search');

        $records["data"] = array();
        $iTotalRecords = Snippets::count();
        $iTotalFiltered = $iTotalRecords;
        $iDisplayLength = intval(Input::get('length')) <= 0 ? $iTotalRecords : intval(Input::get('length'));
        $iDisplayStart = intval(Input::get('start'));
        $sEcho = intval(Input::get('draw'));

        $records["data"] = Snippets::select('*');

        if (!empty($search['value'])) {
            $val = $search['value'];

            $records["data"]->where(function($query) use ($val) {
                $query->SearchTitle($val);
                $query->SearchDescription($val);
            });

        // No of record after filtering
            $iTotalFiltered = $records["data"]->where(function($query) use ($val) {
                $query->SearchTitle($val);
                $query->SearchDescription($val);
            })->count();
        }

        //order by
        foreach ($order as $o) {
            $records["data"] = $records["data"]->orderBy($columns[$o['column']], $o['dir']);
        }

        $records["data"] = $records["data"]->take($iDisplayLength)->offset($iDisplayStart)->get();

        if (!empty($records["data"])) {
            foreach ($records["data"] as $key => $_records) {
                $edit = route('snippet.edit', $_records->id);
                $Image = [];
                if($_records->photo != '' && File::exists(public_path($this->snippetThumbImageUploadPath . $_records->photo))){
                    $Image = '<img src="' . url($this->snippetThumbImageUploadPath . $_records->photo) . '"  height="50" width="50"/>';
                }
                $records["data"][$key]['photo'] = ($Image);
                $records["data"][$key]['action'] = "&emsp;<a href='{$edit}' title='Edit Snippets' ><span class='glyphicon glyphicon-edit'></span></a>
                                                    &emsp;<a href='javascript:;' data-id='" . $_records->id . "' class='btn-delete-ads' title='Delete Snippets' ><span class='glyphicon glyphicon-trash'></span>";
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalFiltered;

        return Response::json($records);
        
    }

    public function editSnippets($id)
    {
       $editSnippets = Snippets::find($id);
        if (!$editSnippets) {
            return Redirect::to("/admin/addsnippets/")->with('error', trans('adminmsg.snippets_not_exist'));
        }
        $snippetsUploadImage = $this->snippetThumbImageUploadPath;
        return view('admin.addSnippets', compact('editSnippets', 'snippetsUploadImage'));
    }
    
    public function saveSnippets()
    {
        $inputData = Input::all();

        $hiddenImage = Input::get('hidden_image');
        $inputData['file'] = $hiddenImage;
        //
        if (Input::file()) {
            $file = Input::file('file');

            if (!empty($file)) {
                $imageType = array('image/jpeg', 'image/png', 'image/jpg', 'image/jpe');
                if (in_array($file->getMimeType(), $imageType)) {
                    $filename = 'sni_' . str_random(20) . '.' . $file->getClientOriginalExtension();
                    $pathOriginal = public_path($this->snippetOriginalImageUploadPath . $filename);
                    $pathThumb = public_path($this->snippetThumbImageUploadPath . $filename);
                    if (!file_exists(public_path($this->snippetOriginalImageUploadPath)))
                        File::makeDirectory(public_path($this->snippetOriginalImageUploadPath), 0777, true, true);
                    if (!file_exists(public_path($this->snippetThumbImageUploadPath)))
                        File::makeDirectory(public_path($this->snippetThumbImageUploadPath), 0777, true, true);

                    // created instance
                    $img = Image::make($file->getRealPath());

                    $img->save($pathOriginal);

                    // resize the image to a height of $this->adsThumbImageHeight and constrain aspect ratio (auto width)
                    $height = ($img->height() < 500) ? $img->height() : $this->snippetsThumbImageHeight;
                    
                    $img->resize(null, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($pathThumb);
                    
                    if ($hiddenImage != '' && $hiddenImage != 'default.png') {
                        $imageOriginal = public_path($this->snippetOriginalImageUploadPath . $hiddenImage);
                        $imageThumb = public_path($this->snippetThumbImageUploadPath . $hiddenImage);
                        
                        // Unlink original file if exist
                        if (file_exists($imageOriginal)) {
                            File::delete($imageOriginal);
                        }
                        
                        // Unlink thumb file if exist
                        if (file_exists($imageThumb)) {
                            File::delete($imageThumb);
                        }
                        
                    }
                    $inputData['photo'] = $filename;
                }
                
            }
        }
        //print_r($inputData);die;
        if (isset($inputData['id']) && $inputData['id'] > 0) {
            $snipp->name = $inputData['name'];
            $snipp->photo = $inputData['photo'];
            $snipp->save();
            return Redirect::to("/admin/ads/")->with('success', trans('adminmsg.ads_updated_success'));
        } else {
            $this->objSnippets->create($inputData);
            return Redirect::to("/admin/snippets/")->with('success', trans('adminmsg.ads_created_success'));
        }
        //

        $modelobj = new Snippets();
        $snippetsData = $modelobj->saveSnippets($inputData);
        if (!$snippetsData) {
            return Redirect::to("/admin/snippets/")->with('error', trans('adminmsg.common_error_msg'));
        }else{
            return Redirect::to("/admin/snippets/")->with('success', trans('adminmsg.snippets_created_success'));
        }
        // $adsUploadImage = $this->adsThumbImageUploadPath;
        // return view('admin.add-ads', compact('editAds', 'adsUploadImage'));
    }
    
}
