<?php 
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;

class Snippets extends Model 
{
    use Notifiable;

    //use SoftDeletes;

    protected $table = "snippets";

	protected $fillable = [
	'title', 'photo','description','html','css'
	];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function getSnippetsData()
    {
    	$snippets = DB::table('snippets')->where('status',1)->get();
        // $data = DB::table('sn')->where('id', $id)->get();
        return $snippets;
    }
    public function saveSnippets($formData)
    {
       $query = Snippets::create($formData);
       return $query;
    }

    public function scopeSearchTitle($query, $value)
    {
        return $query->Where('title', 'LIKE', "%$value%");
    }

    public function scopeSearchDescription($query, $value)
    {
        return $query->Where('description', 'LIKE', "%$value%");
    }

}
