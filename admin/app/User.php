<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Input;
use Auth;
use Image;
use Mail;
use Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use  Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Facades\File;

class User extends Authenticatable
{
    use Notifiable;

/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $table = "user";
protected $fillable = [
    'name', 'email','phone','password','address','propic','gender'
];

/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
protected $hidden = [
    'password',
];
    public function register($data)
    {
        $email = DB::table('user')->where('email',$data['email'])->count();
        $result = "";
        if($email>0)
        {
            return false;
        }
        else
        {
            $data['password'] = Hash::make($data['password']);
            $result = User::create($data);
            return $result;
        }

    }

    public function login($data)
    {
        $user = User::where('email', $data['email'])->first();
        
        if( !is_null($user) && Hash::check($data['password'], $user->password))
        {
            $apikey = base64_encode(str_random(40));
            //User::where('email', $data['email'])->update(['api_key' => "$apikey"]);
            $user->api_key = $apikey;
            $user->save();
            return $user; 
        }
        else
        {
            return false;
        }
    }
    public function forgotPassword($data)
    {
        $user = User::where('email', $data['email'])->first();
        // print_r($user);die;
            if(isset($user) && !empty($user))
            {
                // --------------------start sending mail -----------------------------//
                        $replaceArray = array();
                        $replaceArray['USER_NAME'] = $user['name'];
                        $id = $user['id'];
                        // $replaceArray['USER_UNIQUEID'] = Helpers::getUserUniqueId();
                        // $replaceArray['USER_URL'] = url("resetUserPassword" . "/" . $replaceArray['USER_UNIQUEID']);

                        // $emailTemplateContent = $this->TemplateRepository->getEmailTemplateDataByName(Config::get('constant.USER_RESET_EMAIL_TEMPLATE_NAME'));
                        // $content = $this->TemplateRepository->getEmailContent($emailTemplateContent->et_body, $replaceArray);
                        $data = array();
                        $data['subject'] = "subject";
                        $data['toEmail'] = $user['email'];
                        $data['toName'] = $user['name'];
                        $data['content'] = "content";
                        // $data['USER_UNIQUEID'] = $replaceArray['USER_UNIQUEID'];
                        // $data['user_url'] = $replaceArray['USER_URL'];
                        $data['user_id'] = $id;
                        // $data['is_agent'] = $isAgent;
                        Mail::send(['html' => '<html><body><h6>hello</h6></body></html>'], $data, function($message) use ($data) {
                            $message->subject($data['subject']);
                            $message->to($data['toEmail'], $data['toName']);
                            // $useruniqueid = [];
                            // $useruniqueid['urp_uniquecode'] = $data['USER_UNIQUEID'];
                            // $useruniqueid['urp_user_id'] = $data['user_id'];
                            // $useruniqueid['urp_is_agent'] = $data['is_agent'];
                            // $this->UserRepository->addUserResetPassword($useruniqueid);
                        });
                        // ------------------------end sending mail ----------------------------//
                return true; 
            }
            else
            {
                die("else");
                return false;
            }
    }

    public function getUserData($id)
    {
        $data = DB::table('user')->where('id', $id)->get();
        return $data;
    }

    public function editUserData($data)
    {
        $update = DB::table('user')->where('id', $data['id'])->update(['name' => $data['name'],'email' => $data['email'], 'phone' => $data['phone'], 'address' => $data['address']]);
        if($update)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getTaskDetails()
    {
        $data = DB::table('task')->get();
        return $data;
    }

    public function addTaskData($value)
    {
        $result = DB::table('task')->insert($value);
        if($result)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getTaskData($id)
    {
        $data = DB::table('task')->where('task_id', $id)->get();
        return $data;
    }

    public function editTaskData($data)
    {
        $update = DB::table('task')->where('task_id', $data['task_id'])->update(['name' => $data['name'],'title' => $data['title'], 'status' => $data['status']]);
        if($update)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function deleteTaskData($id)
    {
        $delete = DB::table('task')->where('task_id',$id)->delete();
        if($delete)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
