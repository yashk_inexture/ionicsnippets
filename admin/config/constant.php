<?php
return [

    'ACTIVE_FLAG' => '1',
    'INACTIVE_FLAG' => '2',
    'DELETED_FLAG' => '3',
    'UPLOAD_PATH' => 'uploads/',
    'USER_THUMB_IMAGE_UPLOAD_PATH' => 'uploads/user/thumb/',
    'USER_THUMB_IMAGE_HEIGHT' => '300',
    'USER_THUMB_IMAGE_WIDTH' => '300',  
    //snippet image uploads
    'SNIPPETS_ORIGINAL_IMAGE_UPLOAD_PATH' => 'upload/snippets/original/',
    'SNIPPETS_THUMB_IMAGE_UPLOAD_PATH' => 'upload/snippets/thumb/',
    'SNIPPETS_THUMB_IMAGE_HEIGHT' => '500',
    'SNIPPETS_THUMB_IMAGE_WIDTH' => '500',   
];
