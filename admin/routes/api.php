<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('/', function () use ($app) 
// {
//     return Route::version();
// });

Route::post('registerapi','Api\ExampleController@register');
Route::post('loginapi','Api\ExampleController@login');

Route::post('snippets','Api\SnippetsController@snippets');
Route::post('edit_profile','Api\ExampleController@editprofile');
Route::post('edit_task','Api\ExampleController@edittask');
Route::post('delete_task','Api\ExampleController@deletetask');
Route::post('change_password','Api\ExampleController@changePassword');
Route::post('forgot_password','Api\ExampleController@forgotPassword');
Route::post('add_task','Api\ExampleController@addTask');
Route::get('get_profile','Api\ExampleController@profile');
Route::get('get_task','Api\ExampleController@getTask');
Route::get('get_task_details','Api\ExampleController@getTaskDetails');
