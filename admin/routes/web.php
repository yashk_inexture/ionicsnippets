<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/', 'Admin\LoginController@showLoginForm');
Route::post('/admin/login', 'Admin\LoginController@login');
Route::any('admin/logout', 'Admin\LoginController@logout');
Route::get('admin/home', 'Admin\HomeController@index');

//snippets module
Route::get('admin/snippets', 'Admin\SnippetsController@index');
Route::get('admin/addsnippets', 'Admin\SnippetsController@addSnippets');

Route::post('admin/list-snippet-ajax', 'Admin\SnippetsController@listSnippetAjax');
Route::get('admin/edit-snippets/{id}', array('as' => 'snippet.edit', 'uses' => 'Admin\SnippetsController@editSnippets'));

Route::post('admin/save-snippets', 'Admin\SnippetsController@saveSnippets');

Route::get('/admin/forgotPassword', 'Admin\LoginController@forgotPassword');
Route::post('/admin/forgotPassword', 'Admin\LoginController@sendPassword');
Route::get('/admin/resetPassword/{token}', 'Admin\LoginController@resetPassword');
Route::post('/admin/resetPassword/', 'Admin\LoginController@setNewPassword');

Route::get('user/', 'User\LoginController@showLoginForm' );
Route::post('/user/login', 'User\LoginController@login');
Route::any('user/logout', 'User\LoginController@logout');
Route::get('user/home', 'User\HomeController@index');